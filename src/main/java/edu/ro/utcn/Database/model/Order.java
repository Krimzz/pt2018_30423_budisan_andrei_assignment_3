package edu.ro.utcn.Database.model;

public class Order {
    private int id;
    private int customer_id;
    private int price;

    public Order(){

    }

    public Order(int id, int customer_id, int price) {
        super();
        this.id = id;
        this.customer_id = customer_id;
        this.price = price;
    }

    public Order(int customer_id, int price) {
        super();
        this.customer_id = customer_id;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order [id=" + id + ", customer_id=" + customer_id + ", price=" + price + "]";
    }
}
