package edu.ro.utcn.Database.GUI;

import com.jfoenix.controls.*;
import edu.ro.utcn.Database.bll.ClientBLL;
import edu.ro.utcn.Database.bll.OrderBLL;
import edu.ro.utcn.Database.bll.OrderItemBLL;
import edu.ro.utcn.Database.bll.ProductBLL;
import edu.ro.utcn.Database.connection.ConnectionFactory;
import edu.ro.utcn.Database.model.Client;
import edu.ro.utcn.Database.model.Order;
import edu.ro.utcn.Database.model.OrderItem;
import edu.ro.utcn.Database.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import java.sql.Connection;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable{

    @FXML
    private JFXTextField clientID;

    @FXML
    private JFXTextField clientAge;

    @FXML
    private JFXTextField clientName;

    @FXML
    private JFXTextField clientEmail;

    @FXML
    private JFXTextField clientAddress;

    @FXML
    private JFXButton clientInsert;

    @FXML
    private JFXButton clientDelete;

    @FXML
    private JFXButton clientUpdate;

    @FXML
    private JFXButton clientTableUpdate;

    @FXML
    private TableView<Client> clientTable;

    @FXML
    private TableColumn<Client, Integer> clientCID;

    @FXML
    private TableColumn<Client, String> clientCName;

    @FXML
    private TableColumn<Client, String> clientCAddress;

    @FXML
    private TableColumn<Client, String> clientCEmail;

    @FXML
    private TableColumn<Client, Integer> clientCAge;

    @FXML
    private JFXTextField productID;

    @FXML
    private JFXTextField productName;

    @FXML
    private JFXTextField productQuantity;

    @FXML
    private JFXTextField productPrice;

    @FXML
    private JFXButton productInsert;

    @FXML
    private JFXButton productDelete;

    @FXML
    private JFXButton productUpdate;

    @FXML
    private JFXButton productTableUpdate;

    @FXML
    private TableView<Product> productTable;

    @FXML
    private TableColumn<Product, Integer> productCID;

    @FXML
    private TableColumn<Product, String> productCName;

    @FXML
    private TableColumn<Product, Integer> productCQuantity;

    @FXML
    private TableColumn<Product, Integer> productCPrice;

    @FXML
    private TableView<Order> orderTable;

    @FXML
    private TableColumn<Order, Integer> orderCID;

    @FXML
    private TableColumn<Order, Integer> orderCCustomerID;

    @FXML
    private TableColumn<Order, Integer> orderPrice;

    @FXML
    private JFXTextField orderCustomerID;

    @FXML
    private JFXTextField orderProductID;

    @FXML
    private JFXTextField orderQuantity;

    @FXML
    private JFXButton orderCreate;

    @FXML
    private JFXButton orderTableUpdate;

    @FXML
    private Label underStockErr;

    @FXML
    public void handleButtonAction(MouseEvent event) {
        if (event.getSource() == clientInsert) {
            ClientBLL clientBll = new ClientBLL();
            Client client1 = new Client(clientName.getText(),clientAddress.getText(),clientEmail.getText(),returnInt(clientAge.getText()));
            clientBll.insertClient(client1);
        }
        if (event.getSource() == clientDelete) {
            ClientBLL clientBll = new ClientBLL();
            Client client1 = new Client(returnInt(clientID.getText()),clientName.getText(),clientAddress.getText(),clientEmail.getText(),returnInt(clientAge.getText()));
            clientBll.deleteClient(client1);
        }
        if (event.getSource() == clientUpdate) {
            ClientBLL clientBll = new ClientBLL();
            Client client1 = new Client(returnInt(clientID.getText()),clientName.getText(),clientAddress.getText(),clientEmail.getText(),returnInt(clientAge.getText()));
            clientBll.updateClient(client1);
        }
        if (event.getSource() == clientTableUpdate) {
            getClientsTable();
        }
        if (event.getSource() == productInsert) {
            ProductBLL productBLL = new ProductBLL();
            Product product = new Product(productName.getText(),returnInt(productQuantity.getText()),returnInt(productPrice.getText()));
            productBLL.insertProduct(product);
        }
        if (event.getSource() == productDelete) {
            ProductBLL productBLL = new ProductBLL();
            Product product = new Product(returnInt(productID.getText()),productName.getText(),returnInt(productQuantity.getText()),returnInt(productPrice.getText()));
            productBLL.deleteProduct(product);
        }
        if (event.getSource() == productUpdate) {
            ProductBLL productBLL = new ProductBLL();
            Product product = new Product(returnInt(productID.getText()),productName.getText(),returnInt(productQuantity.getText()),returnInt(productPrice.getText()));
            productBLL.updateProduct(product);
        }
        if (event.getSource() == productTableUpdate) {
            getProductTable();
        }
        if (event.getSource() == orderCreate) {
            ProductBLL productBLL = new ProductBLL();
            OrderItemBLL orderItemBLL = new OrderItemBLL();
            OrderBLL orderBLL = new OrderBLL();

            if(productBLL.findProductById(returnInt(orderProductID.getText())).getQuantity() > returnInt(orderQuantity.getText())) {
                underStockErr.setVisible(false);

                Order order = new Order(returnInt(orderCustomerID.getText()), 0);
                orderBLL.insertOrder(order);

                OrderItem orderItem = new OrderItem(orderBLL.findLastID(), returnInt(orderProductID.getText()), returnInt(orderQuantity.getText()));
                orderItemBLL.insertOrderItem(orderItem);

            }else {
                underStockErr.setVisible(true);
            }
        }
        if (event.getSource() == orderTableUpdate) {
            getOrderTable();
        }

    }

    private ObservableList<Client> clients;
    private ObservableList<Product> products;
    private ObservableList<Order> orders;

    private Connection connection;

    public void getClientsTable(){
        connection = ConnectionFactory.getConnection();
        clients = FXCollections.observableArrayList();
        ClientBLL clientBLL = new ClientBLL();
        for(Client client: clientBLL.findAll()){
            clients.add(client);
        }
        clientCID.setCellValueFactory(new PropertyValueFactory<Client,Integer>("id"));
        clientCName.setCellValueFactory(new PropertyValueFactory<Client,String>("name"));
        clientCAddress.setCellValueFactory(new PropertyValueFactory<Client,String>("address"));
        clientCEmail.setCellValueFactory(new PropertyValueFactory<Client,String>("email"));
        clientCAge.setCellValueFactory(new PropertyValueFactory<Client,Integer>("age"));

        clientTable.setItems(null);
        clientTable.setItems(clients);
    }

    public void getProductTable(){
        connection = ConnectionFactory.getConnection();
        products = FXCollections.observableArrayList();
        ProductBLL productBLL = new ProductBLL();
        for (Product product: productBLL.findAll()){
            products.add(product);
        }
        productCID.setCellValueFactory(new PropertyValueFactory<Product,Integer>("id"));
        productCName.setCellValueFactory(new PropertyValueFactory<Product,String>("name"));
        productCQuantity.setCellValueFactory(new PropertyValueFactory<Product,Integer>("quantity"));
        productCPrice.setCellValueFactory(new PropertyValueFactory<Product,Integer>("price"));

        productTable.setItems(null);
        productTable.setItems(products);
    }

    public void getOrderTable(){
        connection = ConnectionFactory.getConnection();
        orders = FXCollections.observableArrayList();
        OrderBLL orderBLL = new OrderBLL();
        for(Order order: orderBLL.findAll()){
            orders.add(order);
        }
        orderCID.setCellValueFactory(new PropertyValueFactory<Order,Integer>("id"));
        orderCCustomerID.setCellValueFactory(new PropertyValueFactory<Order,Integer>("customer_id"));
        orderPrice.setCellValueFactory(new PropertyValueFactory<Order,Integer>("price"));

        orderTable.setItems(null);
        orderTable.setItems(orders);
    }

    public int returnInt(String text){
        return Integer.parseInt(text);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        getClientsTable();
        getProductTable();
        getOrderTable();
        underStockErr.setVisible(false);
    }


}
