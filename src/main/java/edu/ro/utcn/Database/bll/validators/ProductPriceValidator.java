package edu.ro.utcn.Database.bll.validators;

import edu.ro.utcn.Database.model.Product;

public class ProductPriceValidator implements Validator<Product> {
    private static final int MIN_PRICE = 0;

    @Override
    public void validate(Product t) {

        if (t.getPrice() < MIN_PRICE) {
            throw new IllegalArgumentException("The Product Price smaller than 0!");
        }

    }
}
