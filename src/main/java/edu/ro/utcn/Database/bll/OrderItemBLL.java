package edu.ro.utcn.Database.bll;

import edu.ro.utcn.Database.bll.validators.ProductQuantityValidator;
import edu.ro.utcn.Database.bll.validators.Validator;
import edu.ro.utcn.Database.dao.OrderDAO;
import edu.ro.utcn.Database.dao.OrderItemDAO;
import edu.ro.utcn.Database.dao.ProductDAO;
import edu.ro.utcn.Database.model.Order;
import edu.ro.utcn.Database.model.OrderItem;
import edu.ro.utcn.Database.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class OrderItemBLL {
    private List<Validator<Product>> validators;
    private OrderItemDAO orderItemDAO;

    public OrderItemBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductQuantityValidator());

        orderItemDAO = new OrderItemDAO();
    }

    public OrderItem findOrderItemById(int id) {
        OrderItem st = orderItemDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The orderItem with id =" + id + " was not found!");
        }
        return st;
    }

    public List<OrderItem> findAll() {
        List<OrderItem> orderItems = orderItemDAO.findAll();
        if (orderItems.size() == 0) {
            throw new NoSuchElementException("No elements in table");
        }
        return orderItems;
    }

    public OrderItem insertOrderItem(OrderItem orderItem) {
        ProductDAO productDAO = new ProductDAO();
        Product product = productDAO.findById(orderItem.getProduct_id());
        for (Validator<Product> v : validators) {
            v.validate(product);
        }

        if (orderItem.getQuantity() > product.getQuantity()) {
            throw new IllegalArgumentException("Not enough stock!");
        }

        product.setQuantity(product.getQuantity() - orderItem.getQuantity());
        productDAO.update(product);

        OrderDAO orderDAO = new OrderDAO();
        Order order = orderDAO.findById(orderItem.getOrder_id());
        if (order != null) {
            order.setPrice(order.getPrice() + product.getPrice() * orderItem.getQuantity());
            orderDAO.update(order);
        }

        return orderItemDAO.insert(orderItem);
    }

    public void deleteOrderItem(OrderItem orderItem) {
        orderItemDAO.delete(orderItem);
    }

}
