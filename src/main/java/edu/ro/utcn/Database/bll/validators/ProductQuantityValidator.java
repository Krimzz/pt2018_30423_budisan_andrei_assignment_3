package edu.ro.utcn.Database.bll.validators;

import edu.ro.utcn.Database.model.Product;

public class ProductQuantityValidator implements Validator<Product> {
    private static final int MIN_Q = 0;

    @Override
    public void validate(Product t) {

        if (t.getQuantity() < MIN_Q) {
            throw new IllegalArgumentException("The Product Quantity smaller than 0!");
        }

    }
}
