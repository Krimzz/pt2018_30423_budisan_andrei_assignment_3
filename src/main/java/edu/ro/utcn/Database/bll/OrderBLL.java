package edu.ro.utcn.Database.bll;

import edu.ro.utcn.Database.bll.validators.OrderExistClient;
import edu.ro.utcn.Database.bll.validators.Validator;
import edu.ro.utcn.Database.dao.OrderDAO;
import edu.ro.utcn.Database.dao.ProductDAO;
import edu.ro.utcn.Database.model.Order;
import edu.ro.utcn.Database.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class OrderBLL {

    private List<Validator<Order>> validators;
    private OrderDAO orderDAO;

    public OrderBLL() {
        validators = new ArrayList<Validator<Order>>();
        validators.add(new OrderExistClient());

        orderDAO = new OrderDAO();
    }

    public Order findOrderById(int id) {
        Order st = orderDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }

    public List<Order> findAll(){
        List<Order> orders = orderDAO.findAll();
        if(orders.size()==0){
            throw new NoSuchElementException("No elements in table");
        }
        return orders;
    }

    public Order insertOrder(Order order) {
        for (Validator<Order> v : validators) {
            v.validate(order);
        }
        return orderDAO.insert(order);
    }

    public void deleteOrder(Order order) {
        orderDAO.delete(order);
    }

    public void updateOrder(Order order) {
        for (Validator<Order> v : validators) {
            v.validate(order);
        }
        orderDAO.update(order);
    }

    public int findLastID(){
        int lastID = 0;
        for (Order order: findAll()){
            lastID = order.getId();
        }
        return lastID;
    }
}
