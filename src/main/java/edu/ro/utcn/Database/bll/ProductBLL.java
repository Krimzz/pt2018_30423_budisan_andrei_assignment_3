package edu.ro.utcn.Database.bll;

import edu.ro.utcn.Database.bll.validators.ProductPriceValidator;
import edu.ro.utcn.Database.bll.validators.ProductQuantityValidator;
import edu.ro.utcn.Database.bll.validators.Validator;
import edu.ro.utcn.Database.dao.ProductDAO;
import edu.ro.utcn.Database.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProductBLL {

    private List<Validator<Product>> validators;
    private ProductDAO productDAO;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductPriceValidator());
        validators.add(new ProductQuantityValidator());

        productDAO = new ProductDAO();
    }

    public Product findProductById(int id) {
        Product st = productDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }

    public List<Product> findAll(){
        List<Product> products = productDAO.findAll();
        if(products.size()==0){
            throw new NoSuchElementException("No elements in table");
        }
        return products;
    }

    public Product insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return productDAO.insert(product);
    }

    public void deleteProduct(Product product) {
        productDAO.delete(product);
    }

    public void updateProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        productDAO.update(product);
    }
}
