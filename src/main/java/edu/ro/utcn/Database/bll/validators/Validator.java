package edu.ro.utcn.Database.bll.validators;

public interface Validator<T> {
    public void validate(T t);
}
