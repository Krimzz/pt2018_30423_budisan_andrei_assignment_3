package edu.ro.utcn.Database.bll;

import edu.ro.utcn.Database.bll.validators.ClientEmailValidator;
import edu.ro.utcn.Database.bll.validators.ClientAgeValidator;
import edu.ro.utcn.Database.bll.validators.Validator;
import edu.ro.utcn.Database.dao.ClientDAO;
import edu.ro.utcn.Database.model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ClientBLL {

    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new ClientEmailValidator());
        validators.add(new ClientAgeValidator());

        clientDAO = new ClientDAO();
    }

    public Client findClientById(int id) {
        Client st = clientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return st;
    }

    public List<Client> findAll(){
        List<Client> clients = clientDAO.findAll();
        if(clients.size()==0){
            throw new NoSuchElementException("No elements in table");
        }
        return clients;
    }

    public Client insertClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return clientDAO.insert(client);
    }

    public void deleteClient(Client client) {
        clientDAO.delete(client);
    }

    public void updateClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        clientDAO.update(client);
    }

}
