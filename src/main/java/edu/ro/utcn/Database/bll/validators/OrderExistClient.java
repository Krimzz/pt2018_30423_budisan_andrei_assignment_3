package edu.ro.utcn.Database.bll.validators;

import edu.ro.utcn.Database.bll.ClientBLL;
import edu.ro.utcn.Database.model.Client;
import edu.ro.utcn.Database.model.Order;

public class OrderExistClient implements Validator<Order>{

    @Override
    public void validate(Order t) {
        int id = t.getCustomer_id();
        ClientBLL c = new ClientBLL();
        Client client = c.findClientById(id);
        if (client == null) {
            throw new IllegalArgumentException("No client found in database!");
        }

    }
}
