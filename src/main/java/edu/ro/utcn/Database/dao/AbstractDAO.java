package edu.ro.utcn.Database.dao;

import edu.ro.utcn.Database.connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM warehouse.`");
		sb.append(type.getSimpleName());
		sb.append("`");
		if(field.compareTo("") != 0) {
			sb.append(" WHERE " + field + " =?");
		}
		return sb.toString();
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Not Possible" + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public StringBuilder createInsertString(T t){
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT");
		sb.append(" INTO warehouse.`");
		sb.append(type.getSimpleName());
		sb.append("` (");
		String comma = "";
		for(Field field: t.getClass().getDeclaredFields()){
			field.setAccessible(true);
			if(field.getName().compareTo("id")!=0){
				sb.append(comma);
				sb.append(field.getName());
				comma = ",";
			}
		}
		sb.append(") VALUES (");

		comma = "";
		for(Field field: t.getClass().getDeclaredFields()){
			field.setAccessible(true);
			if(field.getName().compareTo("id")!=0) {
				sb.append(comma);
				sb.append("?");
				comma = ",";
			}
		}
		sb.append(")");

		System.out.println(sb);

		return sb;
	}

	public T insert(T t) {
		int i = 0;
		StringBuilder insertStatementString = createInsertString(t);
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString.toString(), Statement.RETURN_GENERATED_KEYS);
			for(Field field: t.getClass().getDeclaredFields()){
				field.setAccessible(true);
				if(field.getName().compareTo("id")!=0) {
					i++;
					if (field.getType() == Integer.TYPE)
						insertStatement.setInt(i, ((Integer) field.get(t)).intValue());
					else
						insertStatement.setString(i, ((String) field.get(t)).toString());
				}
			}
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, t.getClass().getName() + ":insert " + e.getMessage());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return t;
	}

	public StringBuilder createUpdateString(T t){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE warehouse.`");
		sb.append(type.getSimpleName());
		sb.append("` SET ");

		String comma = "";
		for(Field field: t.getClass().getDeclaredFields()){
			field.setAccessible(true);
			if(field.getName().compareTo("id")!=0){
				sb.append(comma);
				sb.append(field.getName());
				sb.append("=?");
				comma = ",";
			}
		}
		sb.append(" WHERE id=?");
		System.out.println(sb);
		return sb;
	}

	public T update(T t) {
		int i = 0;
		StringBuilder updateStatementString = createUpdateString(t);
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		Object value;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString.toString(), Statement.RETURN_GENERATED_KEYS);
			for(Field field: t.getClass().getDeclaredFields()){
				field.setAccessible(true);
				if(field.getName().compareTo("id")!=0) {
					i++;
					value = field.getName();
					if (field.getType() == Integer.TYPE)
						updateStatement.setInt(i, ((Integer) field.get(t)).intValue());
					else
						updateStatement.setString(i, ((String) field.get(t)).toString());
				}
			}
			i++;
			updateStatement.setLong(i, getID(t));
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, t.getClass().getName() + ":insert " + e.getMessage());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return t;
	}

	public StringBuilder createDeleteString(T t){
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE");
		sb.append(" FROM warehouse.`");
		sb.append(type.getSimpleName());
		sb.append("` WHERE id=?");

		return sb;
	}

	public int getID(T t){
		for(Field field: t.getClass().getDeclaredFields()){
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(t);
				if(field.getName().compareTo("id")==0){
					return ((Integer)value).intValue();
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public T delete(T t) {
		StringBuilder deleteStatementString = createDeleteString(t);
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString.toString(), Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setLong(1,getID(t));
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, t.getClass().getName() + ":insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return t;
	}

}
